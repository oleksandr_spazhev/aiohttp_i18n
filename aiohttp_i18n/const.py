import os

DEFAULT_LOCALE: str = os.environ.get('AIOHTTP_I18N_DEFAULT_LOCALE')
