FROM python:3.8.0-buster

ENV CODE_DIR "/opt"
WORKDIR $CODE_DIR

COPY . $CODE_DIR

RUN python setup.py install
RUN pip install -e ".[test]"

CMD ["pytest"]
